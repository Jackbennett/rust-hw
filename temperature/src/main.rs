#![no_std]
#![no_main]
use core::fmt::Write;
use cortex_m_rt as rt;
use panic_halt as _;

use rt::entry;
use rt::exception;

use microbit::hal::delay::Delay;
use microbit::hal::prelude::*;
use microbit::hal::serial;
use microbit::hal::serial::BAUD115200;

use cortex_m_rt::ExceptionFrame;

#[exception]
fn DefaultHandler(_irqn: i16) {}

#[exception]
fn HardFault(_ef: &ExceptionFrame) -> ! {
    loop {}
}
#[entry]
fn main() -> ! {
    if let Some(p) = microbit::Peripherals::take() {
        // access gpio
        let gpio = p.GPIO.split();

        // setup seraial
        let tx_pin = gpio // All GPIO
            .pin24 // Specific pin
            .into_push_pull_output() // Change pinMode by type
            .into(); // Remove specific pin reference PINXX -> PIN
        let rx_pin = gpio.pin24.into_floating_input().into();
        let (mut tx, _) = serial::Serial::uart0(p.UART0, tx_pin, rx_pin, BAUD115200).split();

        // attach clock to delay function
        let mut timer = Delay::new(p.TIMER0);

        let test_temp_voltage = 1302_u16;
        let sensor = voltage_to_c(&test_temp_voltage);

        // Ready
        writeln!(tx, "Setup has completed");

        loop {
            let t: i16 = p.TEMP.temp.read().bits();
            writeln!(
                tx,
                "System: {:?}, Sensor Voltage({}) converts to {}C",
                t, test_temp_voltage, sensor
            );
            timer.delay_ms(250_u32);
        }
    } else {
        panic!("End");
    }
}

// Formulate to convert mV to *C from a TMP36 sensor
fn voltage_to_c(v: &u16) -> f32 {
    let v = *v as f32;
    v - 500.0 / 10.0
}
